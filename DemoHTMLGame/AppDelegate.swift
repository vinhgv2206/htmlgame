//
//  AppDelegate.swift
//  DemoHTMLGame
//
//  Created by Mạc Ảnh on 5/11/20.
//  Copyright © 2020 Yotel..., sjc. All rights reserved.
//

// id App: ca-app-pub-8187637108736011~3533099178
// unit id: ca-app-pub-8187637108736011/6534473514

import UIKit
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        Chartboost.start(withAppId: "5eba2861549ecb0a604509de", appSignature: "957b353f21b192d64498901300aaa61e4d085878", completion: nil)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = HomeViewController()
        window?.makeKeyAndVisible()
        return true
    }

  


}

