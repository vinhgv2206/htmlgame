//
//  HomeViewController.swift
//  DemoHTMLGame
//
//  Created by Mạc Ảnh on 5/11/20.
//  Copyright © 2020 Yotel..., sjc. All rights reserved.
//

import UIKit
import WebKit
import GoogleMobileAds
import Lottie


/// app id: 5eba2861549ecb0a604509de
/// app signal: 957b353f21b192d64498901300aaa61e4d085878

class HomeViewController: UIViewController {
    
    var webview: WKWebView!
    
    var bannerView: GADBannerView!
    
    var adsBannerConstraint: NSLayoutConstraint!
    var webviewBottomConstraint: NSLayoutConstraint!
    
    var loadingView = AnimationView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadURL()
        Chartboost.showInterstitial(CBLocationHomeScreen)
    }
    
    private func initView() {
        self.view.backgroundColor = .white
                
        webview = WKWebView()
        webview.translatesAutoresizingMaskIntoConstraints = false
        webview.navigationDelegate = self
        self.webview.scrollView.contentInsetAdjustmentBehavior = .never
        self.view.addSubview(webview)
        webview.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        webview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        webview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        webviewBottomConstraint = webview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        webviewBottomConstraint.isActive = true
        webview.configuration.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
        webview.alpha = 0
        
        bannerView = GADBannerView()
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(bannerView)
        bannerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bannerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        bannerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        adsBannerConstraint = bannerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 50)
        adsBannerConstraint.isActive = true

        
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(loadingView)
        loadingView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        loadingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        loadingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        loadingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true

    }
    
    /**
     Thực hiện chạy animations loading website.
     */
    func startLoading() {
        // Nếu cần thay animation loading chỉ cần thay tên file json.
        let animation = Animation.named("17987-bounce-loading")
        loadingView.isHidden = false
        loadingView.alpha = 1
        loadingView.animation = animation
        loadingView.loopMode = .autoReverse
        loadingView.play()

    }
    
    /**
     Thực hiện tắt animation load website
     */
    func stopLoading() {
        UIView.animate(withDuration: 0.2, animations: {
            self.loadingView.alpha = 0
            self.loadingView.isHidden = true
            self.webview.alpha = 1
            self.adsBannerConstraint.constant = 0
            self.webviewBottomConstraint.constant = -50
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.loadingView.stop()
        })
    }
    
    
    
    func loadURL() {
        startLoading()
        // index là tên file, website là tên thư mục chứa file index, html là đuôi file.
        let url = Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "website")!
        webview.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        webview.load(request)
    }
}

extension HomeViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.stopLoading()
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
        guard let urlAsString = navigationAction.request.url?.absoluteString.lowercased() else {
            return
        }
        
        print(urlAsString)
//
//        if urlAsString.range(of: "the url that the button redirects the webpage to") != nil {
//        // do something
//        }
     }

    
}
